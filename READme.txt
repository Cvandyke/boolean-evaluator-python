{\rtf1\ansi\ansicpg1252\cocoartf1504\cocoasubrtf760
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0

\f0\fs24 \cf0 Description:\
For this program I created a file that would take in a boolean statement using file io in the following format:  ( T | F ) & T\
\
The program will sort each line into its own boolean statement then use a series of methods to simplify the statement to a single output.  In this case the result is: T or true\
\
The output will write to file in the following format\
Original:\'85\
Translation:\'85\
Result:\'85 \
(Translation is just changing T to True, F to False, | to or, and & to and.\
\
Eval() WAS NOT ALLOWED.\
\
ToDo: Dictionary could be shortened which would improve readability but decrease the speed and increase the memory in the program.  }