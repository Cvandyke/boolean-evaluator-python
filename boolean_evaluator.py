###############################################################################
#
# Filename: boolean_evaluator.py
#
# CS Assignment: Boolean Evaluator
# Author: Chandler Van Dyke
# Date: 9/22/2016
# Section: CST215 MWF HHMM
#
###############################################################################
#
#Algorithm:
#
#A.Get File
#B.Define Variables
#C.Parse File Contents (line by line)
#   1.Translate
#   2.Interpret result
#   3.Write to output file
#
###############################################################################
#IMPORTS
import time #for practice and unneeded drama
import os #dido



#GET THE FILE:
try:
    orig_file = open("input.txt","r") #open the input file named  "input.txt"


except FileNotFoundError: #error handeling
    #let user know whats going on
    print('File "input.txt" does not exist.')
    time.sleep(.5)#for drama and practice
    print('Please create file "input.txt" and run program again')
    exit()




#DEFINE SOME STUFF
out_file = open("output.txt","w")#create a file to write to


#Assining meaning to variables in file with a dictionary ***FOR TRANSLATION
trans_dict = {"~":"not","|":"or","&":"and","T":"True","F":"False"}

#For finding the result
bool_dict = {"( T & F )":"F", "( T | F )":"T", "( T & T )":"T", "( T | T )":"T", "( T & ~ F )": "T",
             "( T | ~ F )":"T", "( T & ~ T )":"F", "( T | ~ T )":"T", "( F & F )":"F", "( F | F )":"F", "( F & T )":"F",
             "( F | T )":"T", "( F | ~ F )":"T", "( F & ~ F )":"F", "( F | ~ T )":"F", "( F & ~ T )":"F", "( ~ T & F )":"F",
             "( ~ T & T )":"F", "( ~ T & ~ F )":"F", "( ~ T & ~ T )":"F", "( ~ T | F )":"F", "( ~ T | T )":"T",
             "( ~ T | ~ F )":"T", "( ~ T | ~ T )":"F", "( ~ F & F )":"F", "( ~ F & T )":"T", "( ~ F & ~ F )":"T",
             "( ~ F & ~ T )":"F", "( ~ F | F )":"T", "( ~ F | T )":"T", "( ~ F | ~ F )":"T", "( ~ F | ~ T )":"T",
             "T & F":"F", "T | F":"T", "T & T":"T", "T | T":"T", "T & ~ F": "T",
             "T | ~ F":"T", "T & ~ T":"F", "T | ~ T":"T", "F & F":"F", "F | F":"F", "F & T":"F", "F | T":"T",
             "F | ~ F":"T", "F & ~ F":"F", "F | ~ T":"F", "F & ~ T":"F", "~ T & F":"F", "~ T & T":"F", "~ T & ~ F":"F",
             "~ T & ~ T":"F", "~ T | F":"F", "~ T | T":"T", "~ T | ~ F":"T", "~ T | ~ T":"F", "~ F & F":"F", "~ F & T":"T",
             "~ F & ~ F":"T", "~ F & ~ T":"F", "~ F | F":"T", "~ F | T":"T", "~ F | ~ F":"T", "~ F | ~ T":"T",
             "~ F":"T", "~ T":"F", "( ~ F )":"T", "( ~ T )":"F", "T":"T", "F":"F", "( T )":"T", "( F )":"F"}



#START THE GOOD STUFF

#FOR EVERY LINE IN ORIGINAL FILE
for line in orig_file:
    result = line
    trans_line = line#reset for each line (no repeating same line)

    if len(line)==1:#skip blank lines
        out_file.write("\n")#write blank lines to file
        continue

    while len(result) > 2:#while the result line is not solved
        for key in bool_dict:#for every key in the bool dictionary
            #find the bool statements that are left
            #replace bool statements with T or F
            result = result.replace(key, bool_dict[key])


    #translate symbols ~,|,& with not, or, and respectively
    for key in trans_dict:
        trans_line = trans_line.replace(key,trans_dict[key])



 #write to new file

    #write origninal line to out_file with format "Original: ..."
    out_file.write("ORIGINAL: {}\n".format(line))


    #write translation to out_file
    out_file.write("TRANSLATION: {}\n".format(trans_line))


    #write result to out_file
    out_file.write("RESULT: {}\n".format(result)+"\n")



#close files when done :)
orig_file.close()
out_file.close()

#for practice and ease of use :) (opens new file)
os.startfile("output.txt")
